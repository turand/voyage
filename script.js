/*variables delaration*/
let sliderItems = document.querySelectorAll(".carousel > .carousel-inner > .carousel-item");
let navItems = document.getElementsByClassName("nav-item");
let count = 0;
const prev = document.querySelector(".carousel .carousel-control-prev");
const next = document.querySelector(".carousel .carousel-control-next");
sliderItems[count].classList.add("active");

/*moving the slider forwards and backwards*/
prev.addEventListener("click", onPrevSlide);
next.addEventListener("click", onNextSlide);

function onNextSlide() {
    sliderItems[count].classList.remove("active");
    count++;
    if (count >= sliderItems.length) {
        count = 0;
    }
    sliderItems[count].classList.add("active");
}
function onPrevSlide() {
    sliderItems[count].classList.remove("active");

    if (count === 0) {
        count = sliderItems.length;
    }
    count--;
    sliderItems[count].classList.add("active");
}

/*Nav-menu swap .active*/
function swapActive(evt) {
    for (let i = 0; i < navItems.length; i++) {
        navItems[i].className = navItems[i].className.replace("active", "");
    }

    evt.currentTarget.className += " active";
}

/*clear field after clicking search*/
function clearInput() {
    let inputs = document.getElementsByClassName("clear-input");
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].value = "";
    }
}

/*Date picker*/
$(function(){
    let elmntId = document.getElementById("datepicker");
    $(elmntId).datepicker();
});

window.onresize = function(){
    let img = document.getElementById('fullsize');
    img.style.width = "100%";
};